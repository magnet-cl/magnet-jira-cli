# JIRA-CLI

This is a command line interface for JIRA.

The main goal is provide access and automate some common tasks in jira
from the command line.

## Installation

For a local installation you can just run `./quickstart.sh`

This application requires nodejs > v8.9.0. (You can check your current version
with `node --version`).

You can run `jira` in any folder, but the first time it will ask you for your
JIRA credentials. If you need to update this information, remove the `~/.jirarc`
file and run `jira` again.

## Usage

`jira [command] [options]`

See `jira <command> --help` to read about a specific subcommand.

#### Get sub-tasks

`jira [options]`

If called without command, it shows a list of active sub-tasks nested under
their parent issue. For every issue, additional information like status,
assignee, reported time and issue key is displayed too.

Options:
```
  -i, --interactive           Allows interactive search using /<text>.
  -n, --results <maxResults>  Ask the api for more results.
  -p, --project <project>     Specify a project key.
  -v, --version               Output the version number.
  -h, --help                  Output usage information.
```

#### Get all project worklogs

`jira worklogs <project>`

Downloads a report of a project worklogs. It iterates over every project issue (epic, stories, tasks, sub-tasks, etc) and downloads all the worklogs for that issue. A report is generated in CSV and saved to the current working directory.

Each line of the CSV represents a worklog of a user, so issues could be duplicated if the user worked multiple times in that issue.

#### Report

`jira report [options] [template]`

Report time on an issue, this creates a new worklog for that issue.
