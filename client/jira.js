const fetch = require('node-fetch');

const maxParallelRequests = 10;
const searchFields = 'id,key,summary,issuetype,assignee,status,progress,parent';
const searchCustomFields = ['Epic Link', 'Epic Name', 'Sprint'];


/**
* Api Client based on the JIRA REST API v2
* https://developer.atlassian.com/cloud/jira/platform/rest/v2/
*/
class Jira {
  constructor(baseUrl, credentials, options) {
    this.baseUrl = baseUrl;
    this.credentials = credentials;
    this.options = options;

    // Dummy for optional logger
    const dummy = () => {};
    this.log = this.options.log || {
      error: dummy, info: dummy, progress: dummy
    };
  }

  /**
  * Gets an object as a json, from the given url, using this.credentials for
  * authentication returning a promise that resolves with the server answer,
  * parsed as json.
  */
  async getJson(url, parameters) {
    return fetch(`${url}?${parameters ? parameters.join('&') : ''}`, {
      headers: {
        Authorization: `Basic ${this.credentials}`
      }
    })
      .then(res => res.json())
      .then((json) => {
        if (json.errorMessages) {
          throw json;
        }
        return json;
      });
  }

  /**
  * Posts an object as a json, to the given url, using this.credentials for
  * authentication and returns a promise that resolves with the server answer,
  * parsed as json.
  */
  async postJson(url, object) {
    return fetch(url, {
      headers: {
        Authorization: `Basic ${this.credentials}`,
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(object)
    })
      .then(res => res.json())
      .then((json) => {
        if (json.errorMessages) {
          throw json;
        }
        return json;
      });
  }

  /**
  * Downloads all issue fields in Jira, both system and custom fields.
  * It keeps an internal cache of fields at this.fields.
  * @return A promise that resolves with an array of fields [{id, key, name..}].
  */
  async getFields() {
    const url = `${this.baseUrl}/rest/api/2/field`;

    // Check cache
    if (this.fields) {
      return this.fields;
    }

    // Cache miss.. downlaod
    this.log.progress('Downloading fields', 0);
    const fields = await this.getJson(url);
    if (!Array.isArray(fields)) {
      throw fields; // It may be an error in json
    }
    this.log.progress('Downloading fields', fields.length, fields.length);

    // Save cache
    this.fields = fields;
    return fields;
  }

  /**
  * Filters the fields that are relevant for the jqlSearch method.
  * @return A promise that resolves with an array of fields [{id, key, name..}].
  */
  async getCustomFields() {
    return this.getFields()
      .then(fields => fields.filter(
        field => searchCustomFields.includes(field.name)
      ));
  }

  /**
  * Performs a jira-query-language search.
  * This endpoint is paginated, so this method iterates every page.
  * @return A promise that resolves when all the issues are downloaded.
  */
  async jqlSearch(jql) {
    // Api endpoint url and parameters
    const url = `${this.baseUrl}/rest/api/2/search`;
    const maxResults = this.options.maxResults || 50;
    const parameters = [];

    // Fields to require
    let fields = searchFields;
    const additionalFields = await this.getCustomFields();
    if (additionalFields.length > 0) {
      fields += `,${additionalFields.map(f => f.id).join(',')}`;
    }

    // Parameter handling
    parameters.push(`jql=${jql}`);
    parameters.push(`maxResults=${maxResults}`);
    parameters.push(`fields=${fields}`);

    // Dictionary of issues: {id: issue{ id, key, fields: {} }}
    const issues = {};

    // Iterate startAt api parameter
    let currentStart = 0;
    let actualResults = 0;
    let totalResults = Infinity;

    /**
    * Fetches all the issues for the current project.
    * It iterates the result pages until every issue is downloaded.
    * @param startAt: Optional - The starting issue for pagination.
    * @return A promise that resolves with a dictionary of issues by id.
    */
    const fetchIssues = async startAt => this.getJson(
      url, [...parameters, `startAt=${startAt || 0}`]
    )
      .then((json) => {
        // Handle error scenario
        if (!json.issues) {
          throw json;
        }

        // Handle empty response
        if (json.issues.length === 0) {
          return issues;
        }

        // Update total results
        if (json.total < totalResults) { totalResults = json.total; }

        // Store every issue in the dictionary
        json.issues.forEach((issue) => {
          issues[issue.id] = issue;
        });

        // Update progress
        actualResults = Object.keys(issues).length;
        currentStart += maxResults;
        this.log.progress(
          'Downloading issues', actualResults, totalResults
        );

        // Recursive condition, fetch more if needed
        if (actualResults < totalResults) {
          return fetchIssues(currentStart);
        }

        // Else: the issues dictionary is completed!
        return issues;
      });

    // Fetch issues with startAt=0
    return fetchIssues(currentStart);
  }

  /**
  * Fetches sub-tasks and their nested parent issues.
  * @return A promise that resolves with a dictionary of issues.
  */
  async getSubTasks(projectKey, Log) {
    let jql = 'issuetype=Sub-task';
    if (projectKey) {
      jql += `+AND+project=${projectKey}`;
    }
    jql += '+AND+Sprint+IN+(openSprints())';
    jql += '+AND+Status+NOT+IN("Backlog", "Done")';
    return this.jqlSearch(jql, Log)
      .then((issues) => {
        this.log.info('');

        // The dictionary is ready, resolve the promise!
        return issues;
      });
  }

  /**
  * Fetches worklogs for an issue. The result is stored in a .worklogs
  * property in the issue when the promise is resolved.
  * @return Promise that resolves when the worklogs are loaded in the issue.
  */
  async fetchIssueWorklogs(issue) {
    const url = `${this.baseUrl}/rest/api/2/issue/${issue.id}/worklog`;

    return this.getJson(url, ['maxResults=1048576'])
      .then((json) => {
        // Handle error conditions
        if (!json.worklogs) {
          throw json;
        }

        // Store worklogs in the parent issue
        issue.worklogs = json.worklogs;

        return issue;
      });
  }

  /**
  * Fetches worklogs and their parent issues for all issues of a given project.
  * @return A promise that resolves with a dictionary of issues by id
  * and their nested worklogs.
  */
  async getWorklogs(projectKey) {
    this.log.progress('Downloading issues', 0);

    // Search parameters
    const jql = `project=${projectKey}`;

    // Start search
    return this.jqlSearch(jql)
      .then(async (results) => {
        this.log.progress('Downloading worklogs', 0);
        // Fetch worklogs for each issue
        const issuesArray = Object.values(results);
        const totalIssues = issuesArray.length;

        for (let i = 0; i < totalIssues; ++i) {
          this.log.progress('Downloading worklogs', i, totalIssues);
          const parallelRequests = [];

          for (let r = 0; r < maxParallelRequests; ++r) {
            if (i + r < totalIssues) {
              parallelRequests.push(
                this.fetchIssueWorklogs(issuesArray[i + r])
              );
            }
          }

          // "...[Await in] loops may be used to prevent your code from
          // sending an excessive amount of requests in parallel..."
          // eslint-disable-next-line no-await-in-loop
          await Promise.all(parallelRequests);
          i += maxParallelRequests - 1;
        }
        // Notify completion
        this.log.progress('Downloading worklogs', totalIssues, totalIssues);

        return results;
      });
  }

  /**
  * Creates a new worklog for a given issue.
  * @return A promise that resolved when the worklog is created.
  */
  async createWorklog(issueKey, started, timeSpent) {
    const url = `${this.baseUrl}/rest/api/2/issue/${issueKey}/worklog`;
    const worklog = {
      started: `${started}T12:00:00.000+0000`, // Jira requires milliseconds!
      timeSpent
    };

    return this.postJson(url, worklog);
  }
}

module.exports = Jira;
