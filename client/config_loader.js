const chalk = require('chalk');
const fs = require('fs');
const homedir = require('os').homedir();
const inquirer = require('inquirer');
const path = require('path');
const Log = require('../utils/log');
const Printer = require('../utils/printer');

const configPath = path.join(homedir, '.jirarc');
const config = {
  credentials: '',
  baseUrl: '',

  // See https://www.npmjs.com/package/chalk for available formats:
  colors: {
    error: ['bold', 'red'],
    info: ['reset'],
    highlight: ['bold', 'cyan'],
    warn: ['bold', 'yellow'],
    success: ['bold', 'green'],

    issueType: ['cyan'],
    issueStatus: ['gray'],
    issueAssignee: ['gray'],
    issueProgress: ['gray'],
    issueKey: ['green'],
    issueTree: ['gray'],

    searchNormal: ['white'],
    searchMatch: ['cyan'],
    searchHighlight: ['black', 'bgYellow']
  }
};

/**
* Converts the color config into chalk function calls.
* Stores a reference to the chalk format for each key in config.colors into
* config.format, using the same keys, so config.format.error(), .info(), etc.,
* are valid chalk methods.
*/
const createFormatters = () => {
  config.format = {};
  Object.entries(config.colors).forEach(([key, formats]) => {
    let formatter = chalk;
    formats.forEach((format) => {
      formatter = formatter[format];
    });
    config.format[key] = formatter;
  });

  // This is a weird dependency, but it's easier and cleaner to
  // do this here than in every place this config is required.
  Log.configure(config.format);
  Printer.configure(config.format);
};

/**
* User input required to build the config file
*/
const questions = [
  {
    type: 'input',
    name: 'baseUrl',
    message: 'Please enter your JIRA url',
    validate: (url) => {
      if (/^https:\/\/.*$/.test(url)) {
        return true;
      }
      return 'Please enter a valid url';
    },
    default: 'example: https://organization-name.atlassian.net'
  },
  {
    type: 'input',
    name: 'user',
    message: 'Please enter your JIRA user (email)',
    validate: user => /^.+$/.test(user) || 'Please enter a valid user'
  },
  {
    type: 'password',
    name: 'password',
    message: 'Please enter an API token '
      + '(from: https://id.atlassian.com/manage/api-tokens)',
    validate: pass => /^.+$/.test(pass) || 'Please enter a valid token'
  }
];

/**
* Loads the config file from the configPath.
* Asks for user input and creates a new config file if it can't be found.
* @return A promise that resolves with the loaded config.
*/
const loadConfig = async () => {
  try {
    // Try to load a config file
    const configContents = fs.readFileSync(configPath);
    const storedConfig = JSON.parse(configContents);

    // Combine with base settings:
    Object.assign(config, storedConfig);
    createFormatters();

    // If everything is ok, resolve the promise
    return config;
  } catch (error) {
    // The config can't be loaded, we need to create a new one
    return inquirer.prompt(questions).then((answers) => {
      // Generate credentials: base64('user:password')
      config.credentials = Buffer.from(
        `${answers.user}:${answers.password}`
      ).toString('base64');

      // Store the base url
      config.baseUrl = answers.baseUrl;

      // Write to disk
      Log.info('Writing to ~/.jirarc...');
      fs.writeFileSync(configPath, JSON.stringify(config, null, 2));
      createFormatters();

      // If everything is ok, resolve the promise
      Log.success('Done!');
      return config;
    });
  }
};

module.exports = loadConfig;
