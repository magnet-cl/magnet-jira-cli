const Log = require('../utils/log');
const Jira = require('../client/jira');
const loadConfig = require('../client/config_loader');
const print = require('../utils/printer').printIssues;
const search = require('../utils/interactive_terminal');

const getSubTasks = (options) => {
  const clientOptions = {
    log: Log,
    maxResults: options.results
  };

  loadConfig()
    .then(config => new Jira(config.baseUrl, config.credentials, clientOptions))
    .then(client => client.getSubTasks(options.project))
    .then((issues) => {
      if (options.interactive) {
        search(issues);
      } else {
        print(issues);
      }
    })
    .catch((error) => {
      // Print unhandled errors
      Log.error(error);
      process.exitCode = 1;
    });
};

module.exports = getSubTasks;
