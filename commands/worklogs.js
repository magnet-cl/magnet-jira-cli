const Csv = require('../utils/csv');
const Log = require('../utils/log');
const Jira = require('../client/jira');
const loadConfig = require('../client/config_loader');

const getWorklogs = (project) => {
  const clientOptions = {
    log: Log,
    maxResults: 50
  };
  const fileName = `${project}-${(new Date()).toJSON()
    .replace(/[T:.]/g, '-')
    .replace(/[a-zA-Z]/g, '')}.csv`;

  loadConfig()
    .then(config => new Jira(config.baseUrl, config.credentials, clientOptions))
    .then(async client => ({
      issues: await client.getWorklogs(project),
      customFields: await client.getCustomFields()
    }))
    .then(async (results) => {
      Csv.exportWorklogs(results.issues, results.customFields, fileName);
      Log.info(`Report saved to ${fileName}`, 'Completed');
    })
    .catch((error) => {
      // Print unhandled errors
      Log.error(error);
      process.exitCode = 1;
    });
};

module.exports = getWorklogs;
