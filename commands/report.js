const Log = require('../utils/log');
const Jira = require('../client/jira');
const loadConfig = require('../client/config_loader');
const prepareReport = require('../utils/report_composer').prepareReport;

const reportTime = async (issueOrProject, options) => {
  const clientOptions = {
    log: Log
  };

  const context = {
    jira: null,
    report: null
  };

  const config = await loadConfig();
  context.jira = new Jira(
    config.baseUrl, config.credentials, clientOptions
  );

  const report = await prepareReport(
    issueOrProject,
    Log,
    { jira: context.jira, time: options.time, clientOptions }
  );
  await context.jira.createWorklog(
    report.issueKey,
    report.startDate,
    report.timeSpent
  );
  Log.success('Done!');
};

module.exports = reportTime;
