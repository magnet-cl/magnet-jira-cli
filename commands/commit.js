const spawnSync = require('child_process').spawnSync;
const join = require('path').join;
const fs = require('fs');
const Log = require('../utils/log');
const Jira = require('../client/jira');
const loadConfig = require('../client/config_loader');
const prepareReport = require('../utils/report_composer').prepareReport;

const commitMessageFile = '.git/COMMIT_EDITMSG';

const commit = async (issueOrProject, options) => {
  const clientOptions = { log: Log };
  const context = { jira: null, report: null };
  const config = await loadConfig();
  context.jira = new Jira(
    config.baseUrl, config.credentials, clientOptions
  );

  // Ask git about the repository top level in a new process...
  const topLevelProcess = spawnSync('git', ['rev-parse', '--show-toplevel']);
  if (topLevelProcess.status !== 0) {
    // Most common case: 'not a git repository'
    Log.error(topLevelProcess.stderr.toString());
    return;
  }

  // Ask for user input (issue key and time spent)
  const report = await prepareReport(
    issueOrProject,
    Log,
    { jira: context.jira, time: options.time, clientOptions }
  );

  // Start a normal user commit process...
  const commitProcess = spawnSync('git', ['commit'], { stdio: 'inherit' });
  if (commitProcess.status !== 0) {
    // Check if commit is not aborted
    Log.error('Aborting time report due to aborted commit');
    return;
  }

  // Find where the message was saved
  const commitMessagePath = join(
    topLevelProcess.stdout.toString().trim(),
    commitMessageFile
  );

  // Parse the commit message
  let commitMessage = fs.readFileSync(commitMessagePath)
    .toString().split('\n')
    .filter(l => l[0] !== '#')
    .join('\n');

  // Append issue info:
  report.comment = commitMessage;
  commitMessage += `\n${report.issueKey}`;
  fs.writeFileSync(commitMessagePath, commitMessage);

  // Update commit message
  const ammendProcess = spawnSync('git', [
    'commit',
    '--amend',
    '--no-edit',
    '--no-verify',
    `--file=${commitMessagePath}`
  ]);

  if (ammendProcess.status !== 0) {
    // I don't know why an ammend may fail, so this is unhandled...
    throw new Error(ammendProcess.stderr.toString());
  }

  // If everything is still working, send the report!
  await context.jira.createWorklog(
    report.issueKey,
    report.startDate,
    report.timeSpent
  );

  Log.success('Done!');
};

module.exports = commit;
