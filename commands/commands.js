const Log = require('../utils/log');
/* eslint global-require: off */
/* eslint import/no-dynamic-require: off */

module.exports = {
  exec: async (command, ...args) => {
    try {
      await require(`./${command}`)(...args);
    } catch (error) {
      // Print unhandled errors
      Log.error(error);
      process.exitCode = 1;
    }
  }
};
