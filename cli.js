#!/usr/bin/env node
const program = require('commander');
const Commands = require('./commands/commands');
const { version } = require('./package');


// Argument definitions
// See https://github.com/tj/commander.js for syntax
program
  .description('If called without command, it lists open sub-tasks and their '
  + 'parent issues.\nAdditional information will be required on the first run.'
  + '\n\nSee \'jira <command> --help\' to read about a specific subcommand.')
  .option('-i, --interactive', 'Allows interactive search using /<text>.')
  .option('-n, --results <maxResults>', 'Ask the api for more results.')
  .option('-p, --project <project>', 'Specify a project key.');

program
  .command('worklogs <project>')
  .description('Downloads a report of a project worklogs.')
  .action((project, options) => Commands.exec('worklogs', project, options));

program
  .command('report <issue[WIP]|project>')
  .description('Report time on an issue.')
  .option('-t, --time <time>', '[WIP] Amount of time to report, like 3h20m.')
  .action((issueOrProject, options) => Commands.exec(
    'report', issueOrProject, options
  ));

program
  .command('commit <issue[WIP]|project>')
  .description('Git commit and report time on an issue.')
  .option('-t, --time <time>', '[WIP] Amount of time to report, like 3h20m.')
  .action((issueOrProject, options) => Commands.exec(
    'commit', issueOrProject, options
  ));


// Execution starts here
program
  .version(version, '-v, --version')
  .parse(process.argv);

// No command provided
if (program.args.length === 0) {
  Commands.exec('sub-tasks', program);
}
