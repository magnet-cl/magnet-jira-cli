const fs = require('fs');

const csvHeaders = [
  'issue.issuetype',
  'issue.key',
  'issue.summary',
  'issue.parent.issuetype',
  'issue.parent.key',
  'worklog.timeSpent',
  'worklog.timeSpentSeconds',
  'worklog.started',
  'issueGroup.key',
  'issueGroup.summary',
  'issueGroup.epicLink',
  'issueGroup.epicName'
  // 'issueGroup.sprint'
];

/**
* Converts a datetime from tempo, to a date in local user time zone.
* Datetimes from tempo are stored in the wrong timezone. So if a user reports a
* worklog in 2019-04-01 using tempo, then tempo stores 2019-04-01-00-00-00 UTC,
* and the jira api tanslates this to the user timezone as something like
* '2019-03-31T21:00:00.000-0300'. This translation needs to be reverted to
* get the original tempo date.
*/
const tempoStringToDate = (datetimeString) => {
  if (!datetimeString) {
    return '';
  }

  return new Date(datetimeString).toISOString().substring(0, 10);
};

/**
* Logic required to extract each column data.
* For every field, a (issue, worklog) lambda is defined to compute that field.
*/
const extractors = (customFields, issues) => ({
  // Issue properties extraction
  'issue.issuetype': issue => issue.fields.issuetype.name,
  'issue.key': issue => issue.key,
  'issue.summary': issue => issue.fields.summary,

  // Issue parent extraction
  'issue.parent.issuetype': issue => (
    issue.fields.parent ? issue.fields.parent.fields.issuetype.name : ''
  ),
  'issue.parent.key': issue => (
    issue.fields.parent ? issue.fields.parent.key : ''
  ),

  // Worklogs extraction
  'worklog.started': (issue, worklog) => tempoStringToDate(worklog.started),
  'worklog.timeSpent': (issue, worklog) => worklog.timeSpent,
  'worklog.timeSpentSeconds': (issue, worklog) => worklog.timeSpentSeconds,

  // Fake grouping properties
  'issueGroup.key': issue => (issue.fields.parent || issue).key,
  'issueGroup.summary': issue => (issue.fields.parent || issue).fields.summary,
  'issueGroup.epicLink': (issue) => {
    const fieldId = customFields.find(field => field.name === 'Epic Link').id;
    switch (issue.fields.issuetype.name) {
      case 'Epic':
        return issue.key;
      case 'Sub-task':
        return issues[issue.fields.parent.id].fields[fieldId];
      default:
        return issue.fields[fieldId];
    }
  },
  'issueGroup.epicName': (issue) => {
    const nameField = customFields.find(field => field.name === 'Epic Name').id;
    const linkField = customFields.find(field => field.name === 'Epic Link').id;

    switch (issue.fields.issuetype.name) {
      case 'Epic':
        return issue.fields[nameField];
      case 'Sub-task': {
        const parentIssue = issues[issue.fields.parent.id];
        const parentEpicKey = parentIssue.fields[linkField];
        const epicIssue = Object.values(issues).find(
          epic => epic.key === parentEpicKey
        );
        if (!epicIssue) { return ''; }
        return epicIssue.fields[nameField];
      }
      default: {
        const epicIssue = Object.values(issues).find(
          epic => epic.key === issue.fields[linkField]
        );
        if (!epicIssue) { return ''; }
        return epicIssue.fields[nameField];
      }
    }
  },
  'issueGroup.sprint': (issue) => {
    const fieldId = customFields.find(field => field.name === 'Sprint').id;
    const sprint = /name=([^,]*)/.exec(issue.fields[fieldId]);
    return (sprint && sprint.length > 1) ? sprint[1] : '';
  }
});

/**
* Writes an array of rows to a csv file.
*/
const writeFile = (data, file) => {
  let output = '';
  data.forEach((row) => {
    row.forEach((field) => {
      if (field === null || field === undefined) {
        output += ',';
      } else {
        output += `"${field.toString().replace(/"/g, '""')}",`;
      }
    });

    output += '\r\n';
  });

  fs.writeFileSync(file, output);
};


class CsvExporter {
  /**
  * Exports the worklogs nested in every issue.worklogs array as a row
  * in a csv file. Additional columns can be added from custom fields.
  */
  static exportWorklogs(issues, customFields, file) {
    const rows = [csvHeaders];

    Object.values(issues).forEach((issue) => {
      issue.worklogs.forEach((worklog) => {
        rows.push(csvHeaders.map(
          header => extractors(customFields, issues)[header](issue, worklog)
        ));
      });

      // Manually add issues with no worklogs
      if (issue.worklogs.length === 0) {
        // Create a dummy worklog
        const worklog = { started: '', timeSpent: '', timeSpentSeconds: '' };
        rows.push(csvHeaders.map(
          header => extractors(customFields, issues)[header](issue, worklog)
        ));
      }
    });

    writeFile(rows, file);
  }
}

module.exports = CsvExporter;
