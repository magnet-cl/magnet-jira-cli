/* eslint no-console: off */
const chalk = require('chalk');
const removeDiacritics = require('diacritics').remove;
const treefy = require('./treefy');

const ellipsis = '…';
let format = {
  issueType: chalk.cyan,
  issueStatus: chalk.gray,
  issueAssignee: chalk.gray,
  issueProgress: chalk.gray,
  issueKey: chalk.green,
  issueTree: chalk.gray,
  searchNormal: chalk.reset,
  searchMatch: chalk.cyan,
  searchHighlight: chalk.black.bgYellow
};

/**
* Configures the formatters used to print messages.
*/
const configure = (formatConfig) => {
  format = formatConfig;
};

/**
* Creates the white spaces between fiedls.
* Adds an ellipsis if the string is too long.
* Also adds additional spaces to separate columns.
*/
const rightPad = (str, size) => {
  let result = str;

  if (result.length > size) {
    result = `${result.substring(result, size - 1)}${ellipsis}`;
  } else {
    while (result.length < size) {
      result += ' ';
    }
  }

  return `${result}  `;
};

/**
* Converts a number of seconds to a string.
* For example, 7800 becomes "2h 10m".
* @return A string in Jira time span format.
*/
const parseTime = (number) => {
  let minutes = parseInt(number / 60, 10);

  if (minutes < 60) {
    if (minutes < 10) {
      return `   0${minutes}m`;
    }
    return `   ${minutes}m`;
  }

  const hours = Math.floor(minutes / 60);
  minutes -= (hours * 60);

  if (minutes < 10) {
    return `${hours}h 0${minutes}m`;
  }

  return `${hours}h ${minutes}m`;
};

/**
* Converts a date string like this:
*    2020-02-16T21:00:00.000-0300
* into something like this:
*    2020-02-17
*/
const prettifyIsoDate = (dateStr) => {
  const utcDate = new Date(Date.parse(dateStr));
  const result = utcDate.toISOString().substring(0, 10);
  return result || '';
};

/**
* Returns a colored version of the summary of an issue.
* (Or a normal one if no search was performed).
*/
const getHighlightedSummary = (issue) => {
  const sizeLimit = Math.max(80, process.stdout.columns) - 62;

  // There is no search
  if (!issue.search || !issue.search.matches) {
    return format.searchNormal(
      issue.fields.summary.substring(0, sizeLimit)
      + (issue.fields.summary.length > sizeLimit ? ellipsis : '')
    );
  }

  // Search handling
  let result = '';
  const matchesMap = [];
  const highlightsMap = [];

  // Compute matches bitmap
  issue.search.matches.forEach((match) => {
    if (match.key !== 'fields.summary') { return; }
    match.indices.forEach((index) => {
      for (let i = index[0]; i <= index[1]; ++i) {
        matchesMap[i] = true;
      }
    });
  });

  // Compute highlights bitmap
  const lowerSummary = removeDiacritics(issue.fields.summary).toLowerCase();
  removeDiacritics(issue.search.query).toLowerCase().split(' ')
    .forEach((word) => {
      const index = lowerSummary.indexOf(word);
      if (index >= 0) {
        for (let i = 0; i < word.length; ++i) {
          highlightsMap[index + i] = true;
        }
      }
    });

  // Colorize string!
  for (let i = 0; i < issue.fields.summary.length && i < sizeLimit; ++i) {
    const char = issue.fields.summary[i];
    if (highlightsMap[i]) {
      result += format.searchHighlight(char);
    } else if (matchesMap[i]) {
      result += format.searchMatch(char);
    } else {
      result += format.searchNormal(char);
    }
  }

  // Add ellipsis if necessary
  if (issue.fields.summary.length >= sizeLimit) {
    result += ellipsis;
  }

  return result;
};

/**
* Format an issue for console printing.
* @return A single line string with issue fields in colored columns.
*/
const formatIssue = (issue) => {
  const isSubTask = issue.fields.issuetype.name === 'Sub-task';
  let result = isSubTask ? '  ' : '';

  // Print issue type
  result += format.issueType(
    rightPad(issue.fields.issuetype.name, isSubTask ? 8 : 10)
  );

  // Print issue status
  result += format.issueStatus(rightPad(issue.fields.status.name, 10));

  // Print issue assignee
  if (issue.fields.assignee) {
    result += format.issueAssignee(
      rightPad(issue.fields.assignee.displayName, 10)
    );
  } else {
    result += format.issueAssignee(rightPad('unassigned', 10));
  }

  // Print progress
  if (issue.fields.progress) {
    result += format.issueProgress(
      rightPad(parseTime(issue.fields.progress.progress), 6)
    );
  } else {
    result += format.issueProgress(rightPad('', 6));
  }

  // Print issue key
  result += format.issueKey(rightPad(issue.key, 10));

  // Print summary and box drawing characters
  if (isSubTask) {
    if (issue.isLastOfStory) {
      result += ` ${format.issueTree('└')}  ${getHighlightedSummary(issue)}`;
    } else {
      result += ` ${format.issueTree('├')}  ${getHighlightedSummary(issue)}`;
    }
  } else {
    result += getHighlightedSummary(issue);
  }

  return result;
};


/**
* Format a worklog for console printing.
* @return A single line string with worklog fields in colored columns.
*/
const formatWorklog = (worklog) => {
  let result = '  ';
  result += `${format.issueKey(prettifyIsoDate(worklog.started))}   `;
  result += `${format.issueType(rightPad(worklog.timeSpent, 7))} `;
  result += `${format.issueProgress(rightPad(worklog.comment || '', 50))}`;
  return result;
};

/**
* Returns an array with only the parent issues of a tree,
* ordered by the minimum score of the sub-tree in a search,
* or the id of the parent id if not.
*/
const sortIssues = (issues) => {
  // Order: [{id: .., score: ...}, {id: .., score: ---}, ..]
  const order = [];

  Object.values(issues).forEach((issue) => {
    if (issue.search) {
      // If search was performed, order by search score
      order.push({
        id: issue.id,
        score: issue.subtasks.reduce(
          (acc, st) => Math.min(acc, st.search.score),
          issue.search.score
        )
      });
    } else {
      // If not in a search, order by reverse id order
      order.push({
        id: issue.id,
        score: -parseInt(issue.id, 10)
      });
    }
  });

  // Order by score
  return order.sort((a, b) => b.score - a.score).map(t => issues[t.id]);
};

/**
* Prints the dictionary of issues and their nested sub-tasks.
*/
const printIssues = (issues) => {
  const sortedIssues = sortIssues(treefy(issues));

  // Print in order
  sortedIssues.forEach((issue) => {
    // Dictionary of issues: {id: issue{ subtasks: [{}, ...] }}

    // Mark last sub-task of story
    if (issue.subtasks && issue.subtasks.length > 0) {
      issue.subtasks[issue.subtasks.length - 1].isLastOfStory = true;
    }

    // Print each issue...
    console.log(formatIssue(issue));
    // ...then print the nested sub-tasks
    if (issue.subtasks) {
      issue.subtasks.forEach((subtask) => {
        console.log(formatIssue(subtask));
      });
    }
  });
};

module.exports = {
  printIssues, formatIssue, formatWorklog, configure
};
