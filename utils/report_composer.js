const inquirer = require('inquirer');
const Jira = require('../client/jira');
const format = require('../utils/printer').formatIssue;
const formatWorklog = require('../utils/printer').formatWorklog;
const loadConfig = require('../client/config_loader');
const interactive = require('../utils/interactive_terminal');

// Returns the current date as a YYYY-mm-dd string.
const today = () => {
  const date = new Date();
  const y = date.getFullYear();
  const m = date.getMonth() + 1;
  const d = date.getDate();

  return `${y}-${m < 10 ? '0' : ''}${m}-${d < 10 ? '0' : ''}${d}`;
};

/**
* Interactive build of a time report from user inputs.
* @param issueOrProject issue key string or project key.
* @param log instance of utils/log to format user messages.
* @param options {[time], [jira], [config], [clientOptions]}.
*/
const prepareReport = async (issueOrProject, log, options) => {
  // Context of our time report, some info may be already provided
  const context = {
    time: options.time,
    issueKey: issueOrProject.includes('-') ? issueOrProject : undefined,
    projectKey: issueOrProject.includes('-') ? undefined : issueOrProject,
    issue: undefined,
    jira: options.jira || null,
    config: options.config || null
  };

  // Initialize jira if required...
  if (!context.jira) {
    // ...lLoading the config if required...
    if (!context.config) {
      context.config = await loadConfig();
    }

    // ...and creating a new jira client.
    context.jira = new Jira(
      context.config.baseUrl,
      context.config.credentials,
      options.clientOptions
    );
  }

  // Find the issue...
  // TODO: change jql query if issue key is received
  const issues = await context.jira.jqlSearch(`project=${context.projectKey}`);
  context.issue = await interactive(issues, { select: true });

  // User feedback:
  log.info('', '\nSelected issue:');
  log.raw(format(context.issue));

  log.info('', '\nLast worklogs:');
  await context.jira.fetchIssueWorklogs(context.issue);
  if (context.issue.worklogs.length === 0) {
    log.info('  No worklogs found.');
  } else {
    context.issue.worklogs.forEach((worklog) => {
      log.raw(formatWorklog(worklog));
    });
  }

  // User input
  log.info('', '\nEnter your worklog:');
  const when = today();
  const questions = [
    {
      type: 'input',
      name: 'time',
      message: 'Worked time? (in Jira format, ie 3h25m)',
      validate: (worked) => {
        if (/^(\d+d)?(\d+h)?(\d+m)?$/.test(worked) && worked !== '') {
          return true;
        }
        return 'Please enter a valid time';
      }
    },
    {
      type: 'input',
      name: 'when',
      message: `When was the work done? (default ${when})`,
      validate: (date) => {
        if (/^\d{4}-\d{2}-\d{2}$/.test(date) || date === '') {
          return true;
        }
        return 'Please enter a valid date (YYYY-mm-dd)';
      },
      filter: date => (date === '' ? when : date)
    }
  ];

  const answers = await inquirer.prompt(questions);

  // Jira format, pls... the space is required
  answers.time = answers.time.replace(/d(\d)/, 'd $1');
  answers.time = answers.time.replace(/h(\d)/, 'h $1');

  // Build final report...
  return {
    issueKey: context.issue.key,
    timeSpent: answers.time,
    startDate: answers.when,
    comment: answers.comment || ''
  };
};


module.exports = { prepareReport };
