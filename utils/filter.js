const Fuse = require('fuse.js');
const createTree = require('./treefy');

/**
* Configures Fuse.js to filter our array of issues.
* @return A filtered array of issues.
*/
const fuzzySearch = (issues, query) => {
  const options = {
    shouldSort: true,
    includeScore: true,
    includeMatches: true,
    findAllMatches: false,
    threshold: 0.99,
    location: 0,
    distance: 300,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
      'fields.assignee.displayName',
      'fields.summary'
    ]
  };
  const fuse = new Fuse(Object.values(issues), options);
  return fuse.search(query);
};

/**
* Helper to compute the score of an unknown issue.
*/
const issueScore = issue => (
  issue.search ? issue.search.score : parseInt(issue.id, 10)
);


/**
* Returns a subset of a parent-subtask tree, preserving the parent
* issues, but limited to maxResults issues.
*/
const pruneTree = (tree, maxResults) => {
  // Flatten tree
  const issues = Object.values(tree);
  Object.values(tree).forEach((parent) => {
    if (parent.subtasks) {
      parent.subtasks.forEach((subtask) => {
        issues.push(subtask);
      });
    }
  });

  // Sort issues by score
  issues.sort((a, b) => issueScore(a) - issueScore(b));

  // Find the maximum score that's under the issue limit
  let maxScore = -Infinity;
  let issueCount = 0;
  issues.forEach((issue) => {
    const score = issueScore(issue);
    if (issueCount < maxResults) {
      maxScore = score;
      ++issueCount;
    }
  });

  // Create a copy of the tree with the issues that are under the score limit
  const limitedTree = [];
  issueCount = 0;
  Object.values(tree)
    .sort((a, b) => issueScore(a) - issueScore(b))
    .forEach((parent) => {
      if (issueCount < maxResults && issueScore(parent) <= maxScore) {
        // We need a shallow copy of the parent issue...
        const issueCopy = { ...parent };
        // ...with a different subtask array (a limited one)
        issueCopy.subtasks = [];
        limitedTree.push(issueCopy);
        ++issueCount;

        // Selectively add subtasks
        if (parent.subtasks) {
          parent.subtasks.forEach((subtask) => {
            const score = (
              subtask.search ? subtask.search.score : parseInt(subtask.id, 10)
            );
            if (issueCount < maxResults && score <= maxScore) {
              issueCopy.subtasks.push(subtask);
              ++issueCount;
            }
          });
        }
      }
    });

  return limitedTree;
};

/**
* Filters a tree of issues and returns a new tree with
* the same structure, but excluding the unmatching results.
* @param issues Dictionary of issues.
* @param text Search query.
*/
const filterIssues = (issues, text, maximum) => {
  let issuesArray = [];
  const limit = maximum || Infinity;

  // Flatten issue tree (into issuesArray)
  Object.values(issues).forEach((issue) => {
    const issueCopy = { ...issue }; // shallow copy
    issueCopy.fields = { ...issue.fields }; // shallow copy
    delete issueCopy.subtasks; // clear reference
    if (issue.fields.parent) {
      issue.fields.parent = { ...issue.fields.parent };
    }
    issuesArray.push(issueCopy);
  });

  if (text.length < 1) {
    // Just generate tree if nothing to search
    return pruneTree(createTree(issuesArray), limit);
  }

  // Filter the array
  issuesArray = fuzzySearch(issuesArray, text).map((result) => {
    result.item.search = {
      matches: result.matches,
      query: text,
      score: result.score
    };
    return result.item;
  });

  // Generate tree
  const tree = createTree(issuesArray);
  Object.values(tree).forEach((issue) => {
    // Aggregate subtask score on parent issue
    if (issue.subtasks) {
      // If the issue was removed from search
      if (!issue.search) {
        issue.search = {
          matches: '',
          query: text,
          score: 1 // 0 is good, 1 is bad
        };
      }

      // Aggregate subtasks score. Use the minimum (best)
      issue.search.score = issue.subtasks.reduce((min, subtask) => {
        const score = issueScore(subtask);
        return score < min ? score : min;
      }, issue.search.score);
    }
  });

  return pruneTree(tree, limit);
};

module.exports = filterIssues;
