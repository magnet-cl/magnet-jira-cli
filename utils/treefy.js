const createTree = (issues) => {
  // Dictionary of issues: {id: issue{ subtasks: [{}, ...] }}
  const issuesArray = Object.values(issues);
  const tree = {};

  issuesArray.forEach((issue) => {
    if (issue.fields.issuetype.name === 'Sub-task') {
      // Search for parent issue
      let parent = tree[issue.fields.parent.id];
      if (!parent) {
        // The parent issue is not indexed yet, create now
        parent = issues[issue.fields.parent.id] || issue.fields.parent;
        tree[parent.id] = parent;
        parent.subtasks = [];
      }

      // Include this subtask in a subtasks list in the parent issue
      parent.subtasks.push(issue);
    } else {
      // If it is not a sub-task, add it to the tree only if
      // it's not already added by a subtask.
      const issueInTree = tree[issue.id];
      if (!issueInTree) {
        tree[issue.id] = issue;
        issue.subtasks = [];
      }
    }
  });

  return tree;
};

module.exports = createTree;
