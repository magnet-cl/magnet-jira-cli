const term = require('terminal-kit').terminal;
const filter = require('./filter');
const format = require('./printer').formatIssue;

let currentIndex = 0;
let currentIssue = null;

/**
* Based on a tree of issues, it prints only the best matches against a
* given query, until maxResults is reached.
* @param issues A dictionary of issues. {id: {...issue}, ...}.
* @param query A search string.
* @param maxResults Maximum available lines, used to prune the tree.
*/
const filterPrintIssues = (issues, query, options) => {
  const enableSelection = options.select;
  const maxResults = options.maxResults;
  const filteredIssues = filter(issues, query, maxResults);

  let index = 0;
  Object.values(filteredIssues).forEach((issue) => {
    if (enableSelection && index === currentIndex) {
      currentIssue = issue;
      term.inverse(`${format(issue)}\n`);
    } else {
      term(`${format(issue)}\n`);
    }
    ++index;

    if (issue.subtasks) {
      issue.subtasks.forEach((subtask) => {
        if (enableSelection && index === currentIndex) {
          currentIssue = subtask;
          term.inverse(`${format(subtask)}\n`);
        } else {
          term(`${format(subtask)}\n`);
        }
        ++index;
      });
    }
  });
};

/**
* Blocks the terminal while the user is searching for an issue.
* It activates the alternate screen buffer when searching.
*/
const search = async (issues, options) => new Promise((resolve) => {
  // Setup
  const enableSelection = options ? options.select || false : false;
  let userInput = '/';

  term.fullscreen(true);
  term.grabInput();
  term.moveTo(0, 0)
    .clear()
    .moveTo(0, term.height);
  term(userInput)
    .moveTo(userInput.length + 1, term.height);

  // Handle user input
  term.on('key', (name, matches, data) => {
    const issuesArea = {
      height: term.height - 1,
      width: term.width
    };
    let redrawTree = false;

    // Detect CTRL-C and exit 'manually'
    if (name === 'CTRL_C') {
      term.fullscreen(false);
      process.exit();
    }

    // Detect CTRL-U and clear user input
    if (name === 'CTRL_U') {
      userInput = userInput.length > 0 ? userInput[0] : '';
      redrawTree = true;
    }

    // Detect commands
    if (name === 'ENTER') {
      if (userInput === ':q') {
        term.fullscreen(false);
        process.exit();
      }

      userInput = '';
      term.fullscreen(false);
      term.grabInput(false);
      resolve(currentIssue);
    }

    // Detect clear
    if (name === 'ESCAPE') {
      userInput = '';
    }

    // Detect BACKSPACE
    if (name === 'BACKSPACE') {
      if (userInput.length === 0) {
        return;
      }
      userInput = userInput.substring(0, userInput.length - 1);
      redrawTree = true;
    }

    // Detect arrows
    if (name === 'UP') {
      currentIndex = Math.max(0, currentIndex - 1);
      redrawTree = true;
    }

    if (name === 'DOWN') {
      currentIndex = Math.min(Object.keys(issues).length - 1, currentIndex + 1);
      redrawTree = true;
    }

    // Concatenate
    if (data.isCharacter) {
      // Prevent invalid commands
      if (userInput.length === 0) {
        if (name !== ':' && name !== '/') {
          return;
        }
      }
      userInput += name;

      // Detect search
      if (userInput[0] === '/') {
        redrawTree = true;
      }
    }

    if (redrawTree) {
      term.moveTo(0, 0);
      term.clear();
      // Search code here
      filterPrintIssues(issues, userInput.substring(1), {
        maxResults: issuesArea.height,
        select: enableSelection
      });
    }

    // Re-draw input
    term.moveTo(0, term.height);
    term.eraseLine();
    term(userInput);
    term.moveTo(userInput.length + 1, term.height);
  });

  // Trigger a first draw with an empty search
  term.moveTo(0, 0);
  filterPrintIssues(issues, '', {
    maxResults: term.height - 1,
    select: enableSelection
  });
  term.moveTo(userInput.length + 1, term.height);
});

module.exports = search;
