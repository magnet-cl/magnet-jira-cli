/* eslint no-console: off */
const chalk = require('chalk');

let format = {
  error: chalk.bold.red,
  info: chalk.default,
  highlight: chalk.bold.cyan,
  warn: chalk.bold.yellow,
  success: chalk.bold.green
};

// Automatically pretty-format messages that are not strings
const stringify = (message) => {
  if (typeof message === 'string') {
    return message;
  }

  if (message instanceof Error) {
    return `\n${message.stack}`;
  }

  return JSON.stringify(message, null, 2);
};

class Log {
  /**
  * Configures the formatters used to print messages.
  */
  static configure(formatConfig) {
    format = formatConfig;
  }

  /**
  * Prints unhandled errors.
  */
  static error(error) {
    console.error(format.error(stringify(error)));
  }

  /**
  * Prints normal messages with an optional highlighted title.
  */
  static info(message, title) {
    if (title) {
      console.log(
        `${format.highlight(stringify(title))} ${stringify(message)}`
      );
    } else {
      console.log(format.info(stringify(message)));
    }
  }

  /**
  * Prints a string without processing.
  */
  static raw(string) {
    console.log(string);
  }

  /**
  * Prints a success messages.
  */
  static success(message) {
    console.log(format.success(stringify(message)));
  }

  /**
  * Prints or updates a progress message.
  */
  static progress(message, current, total) {
    // Format output
    let output = format.highlight(stringify(message));
    output += ' ';
    output += format.info(
      total > 0 ? Math.round(1000 * current / total) / 10 : 0
    );
    output += '%';
    if (total > 0) {
      output += ` (${current} / ${total})`;
    }

    // Print updating previous one...
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(output);

    // Add a new line at the end
    if (total !== undefined && current === total) {
      console.log('');
    }
  }
}

module.exports = Log;
